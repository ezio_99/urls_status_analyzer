import asyncio
from pathlib import Path
from collections import Counter
import ssl
from socket import gaierror
from argparse import ArgumentParser
import sys
from datetime import datetime

import aiohttp
from aiohttp.client_exceptions import ClientError
from loguru import logger

MAX_URLS = 100_000

parser = ArgumentParser()
parser.add_argument("-i", "--input", help="File with urls", required=True)
parser.add_argument("-o", "--output", help="Output file", required=True)
parser.add_argument("-m", "--max-urls", help="Max urls for concurrent processing")


def exit(message: str, exit_code: int = 0):
    print(message)
    sys.exit(exit_code)


async def get_status(url: str) -> str:
    timeout = aiohttp.ClientTimeout(total=10)
    async with aiohttp.ClientSession(timeout=timeout) as session:
        try:
            async with session.head(url) as resp:
                status = str(resp.status)
        except asyncio.exceptions.TimeoutError:
            status = "TimeoutError"
        except ClientError:
            status = "ClientError"
        except ssl.SSLCertVerificationError:
            status = "SSLCertVerificationError"
        except gaierror:
            status = "Address not found"
        except:
            status = "Exception"
    return status


async def process_url(url: str, counter: Counter, output_file):
    status = await get_status(url)
    counter[status] += 1
    output_file.write(f"{url} {status}\n")


async def main(tasks: list) -> None:
    await asyncio.wait(tasks, return_when=asyncio.ALL_COMPLETED)


if __name__ == "__main__":
    args = parser.parse_args()

    try:
        input_filename = Path(args.input)
        if not input_filename.exists():
            exit("Input file does not exist")
    except TypeError:
        exit("Invalid input file")

    if args.max_urls is not None:
        try:
            MAX_URLS = int(args.max_urls)
        except ValueError:
            exit("Invalid max_urls value")

    output_filename = Path(args.output)

    counter = Counter()
    now = datetime.now()
    with open(input_filename, "r", encoding="utf-8") as input_file, open(output_filename, "w") as output_file:
        urls = []
        i = 0
        start = 1
        end = MAX_URLS
        for line in input_file:
            stripped = line.strip()
            if stripped:
                urls.append(stripped)
                i += 1
            if i == MAX_URLS:
                tasks = [process_url(url, counter, output_file) for url in urls]
                asyncio.run(main(tasks))
                logger.info("Processed {}-{}", start, end)
                start = end + 1
                end += MAX_URLS
                urls = []
                i = 0
        if urls:
            tasks = [process_url(url, counter, output_file) for url in urls]
            asyncio.run(main(tasks))
            logger.info("Processed {}-{}", start, start + i - 1)
    logger.info("Total time: {}", datetime.now() - now)
