from argparse import ArgumentParser
from pathlib import Path
import sys

parser = ArgumentParser()
parser.add_argument("-i", "--input1", help="File1", required=True)
parser.add_argument("-j", "--input2", help="File2", required=True)


def exit(message: str):
    print(message)
    sys.exit()


def check_path(path_as_str: str) -> Path:
    try:
        path = Path(path_as_str)
        if not path.exists():
            exit(f"{path} does not exist")
        return path
    except TypeError:
        exit(f"Invalid path - {path_as_str}")


def read_file(path: Path, col: int = 0, delimiter: str = " ") -> set:
    data = set()
    with open(path, "r", encoding="utf-8") as f:
        for line in f:
            stripped = line.strip()
            if stripped:
                data.add(stripped.strip(delimiter)[col])
    return data


if __name__ == "__main__":
    args = parser.parse_args()
    input1 = check_path(args.input1)
    input2 = check_path(args.input2)

    data1 = read_file(input1)
    data2 = read_file(input2)

    print(f"{input1} - {input2}:")
    for e in data1 - data2:
        print(e)

    print(f"\n{input2} - {input1}:")
    for e in data2 - data1:
        print(e)
