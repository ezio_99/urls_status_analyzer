1. Creating virtual environment:

    `python3 -m venv venv`

2. Activating virtual environment:

    `source venv/bin/activate`    

3. Installing dependencies:

    `pip3 install -r requirements.txt`
    
4. Running script:

    `python3 main1.py -i ./urls.txt -o ./output.txt`
    
5. Checking correctness:

    `python3 diff.py -i urls.txt -j output.txt`
    
Optionally: you can edit max number of concurrently running processes through `-m` key or in main1.py `MAX_URLS`
