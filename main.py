import asyncio
from pathlib import Path
from collections import Counter
import ssl
from socket import gaierror
from argparse import ArgumentParser
import sys

import aiohttp
from aiohttp.client_exceptions import ClientError


parser = ArgumentParser()
parser.add_argument("-i", "--input", help="File with urls", required=True)
parser.add_argument("-o", "--output", help="Output file", required=True)


def exit(message: str, exit_code: int = 0):
    print(message)
    sys.exit(exit_code)


class Index:
    def __init__(self):
        self.value = 1


async def get_status(url: str) -> str:
    timeout = aiohttp.ClientTimeout(total=10)
    async with aiohttp.ClientSession(timeout=timeout) as session:
        try:
            async with session.head(url) as resp:
                status = str(resp.status)
        except asyncio.exceptions.TimeoutError:
            status = "TimeoutError"
        except ClientError:
            status = "ClientError"
        except ssl.SSLCertVerificationError:
            status = "SSLCertVerificationError"
        except gaierror:
            status = "Address not found"
        except:
            status = "Exception"
    return status


def write_status(url: str, status: str, output) -> None:
    output.write(f"{url} {status}\n")


async def process_url(url: str, counter: Counter, output_file, start_index: Index, done_index: Index):
    print(f"{start_index.value}. Starting process {url}")
    start_index.value += 1

    status = await get_status(url)
    counter[status] += 1
    write_status(url, status, output_file)

    print(f"{done_index.value}. Done {url} - {status}")
    done_index.value += 1


def get_urls(input: Path) -> list:
    with open(input, "r", encoding="utf-8") as f:
        data = []
        for line in f:
            stripped = line.strip()
            if stripped:
                data.append(stripped)
    return data


async def main(input: Path, output: Path, counter: Counter) -> None:
    output_file = open(output, "w")
    try:
        print("Loading urls ...", end=" ")
        urls = get_urls(input)
        print("Done")

        start_index, done_index = Index(), Index()
        tasks = [asyncio.create_task(process_url(url, counter, output_file, start_index, done_index)) for url in urls]
        await asyncio.wait(tasks, return_when=asyncio.ALL_COMPLETED)
    finally:
        output_file.close()


if __name__ == "__main__":
    args = parser.parse_args()

    try:
        input = Path(args.input)
        if not input.exists():
            exit("Input file does not exist")
    except TypeError:
        exit("Invalid input file")

    output = Path(args.output)

    counter = Counter()
    asyncio.run(main(input, output, counter))
